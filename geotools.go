// Package geotools provides conversion and calculation tools for geographical coordinates.
package geotools

import (
	"log"
	"math"
	"strconv"
	"strings"
)

// EarthRadius represents earth radius in meters.
const EarthRadius float64 = 6371e3

// DMStoDD converts a coordinate point from 'Degrees Minutes Seconds' (0° 0' 0.0000" N) notation to 'Decimal Degrees' (0.0).
func DMStoDD(degrees, minutes, seconds, cardinal string) float64 {

	/*
		TODO: are degrees (lat 0-90 and lon 0-180 ) or (lat 0-89 and lon 0-179)
	*/

	var direction, deg, min, sec float64
	var err error

	deg, err = strconv.ParseFloat(degrees, 64)
	if err != nil {
		log.Fatal(err)
	}
	min, err = strconv.ParseFloat(minutes, 64)
	if err != nil {
		log.Fatal(err)
	}
	sec, err = strconv.ParseFloat(seconds, 64)
	if err != nil {
		log.Fatal(err)
	}

	switch strings.ToUpper(cardinal) {
	case "W", "S", "-":
		direction = -1
	case "E", "N":
		direction = 1
	default:
		log.Fatal("Direction should one of N,S,W,E,-")
	}

	return direction * (deg + (min / 60) + (sec / 3600))
}

// Distance calculates the distance of two coordinates points.
// It uses the haversine distance formula from http://www.movable-type.co.uk/scripts/latlong.html
func Distance(lat1, lon1, lat2, lon2 float64) (distance float64) {

	var dLat float64 = (lat2 - lat1) * math.Pi / 180
	var dLon float64 = (lon2 - lon1) * math.Pi / 180
	lat1 = lat1 * math.Pi / 180
	lat2 = lat2 * math.Pi / 180

	var a, c float64

	a = math.Sin(dLat/2)*math.Sin(dLat/2) +
		math.Sin(dLon/2)*math.Sin(dLon/2)*math.Cos(lat1)*math.Cos(lat2)

	c = 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))
	distance = EarthRadius * c

	return distance
}

// Bearing calculates the initial bearing of two coordinates points.
// It uses the bearing formula from http://www.movable-type.co.uk/scripts/latlong.html
func Bearing(lat1, lon1, lat2, lon2 float64) (bearing float64) {

	var dLon float64 = (lon2 - lon1) * math.Pi / 180
	lat1 = lat1 * math.Pi / 180
	lat2 = lat2 * math.Pi / 180

	var x, y float64

	y = math.Sin(dLon) * math.Cos(lat2)
	x = math.Cos(lat1)*math.Sin(lat2) - math.Sin(lat1)*math.Cos(lat2)*math.Cos(dLon)
	bearing = math.Atan2(y, x) * 180 / math.Pi

	return bearing
}
