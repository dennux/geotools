/*
https://www.unece.org/cefact/locode/welcome.html
https://en.wikipedia.org/wiki/UN/LOCODE
*/
package unlocode

import (
	"archive/zip"
	"bitbucket.org/dennux/geotools"
	"encoding/csv"
	"golang.org/x/text/encoding/charmap"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

const (
	csvurl        = "https://www.unece.org/fileadmin/DAM/cefact/locode/loc201csv.zip"
	csvnamepatern = "CodeListPart"
)

var ziptmp string = os.TempDir() + "/" + "unlocode.zip"

type unlocode struct {
	change       string
	countryCode  string
	locationCode string
	name         string
	nameSimple   string
	subdivision  string
	function     string
	status       string
	date         string
	iata         string
	coordinates  string
	remarks      string
}

type DB struct {
	locations          []unlocode
	countries          []unlocode
	errors             []unlocode
	refs               []unlocode
	dups               []unlocode
	pkey               map[string]int
	locations_position int
	countries_position int
}

func init() {
	if _, err := os.Stat(ziptmp); os.IsNotExist(err) {
		download()
	}
}

func download() bool {

	resp, err := http.Get(csvurl)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	out, err := os.Create(ziptmp)
	if err != nil {
		log.Fatal(err)
	}
	defer out.Close()

	_, err = io.Copy(out, resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	return true
}

func (db *DB) load() {
	// Open a zip archive for reading.
	r, err := zip.OpenReader(ziptmp)
	if err != nil {
		log.Fatal(err)
	}
	defer r.Close()

	// Iterate through the files in the archive,
	for _, f := range r.File {
		if strings.Contains(f.Name, csvnamepatern) {
			rc, err := f.Open()
			if err != nil {
				log.Fatal(err)
			}
			defer rc.Close()

			r1 := charmap.ISO8859_1.NewDecoder().Reader(rc)

			r2 := csv.NewReader(r1)
			for {
				record, err := r2.Read()
				if err == io.EOF {
					break
				}
				if err != nil {
					log.Fatal(err)
				}

				u := unlocode{
					change:       record[0],
					countryCode:  record[1],
					locationCode: record[2],
					name:         record[3],
					nameSimple:   record[4],
					subdivision:  record[5],
					function:     record[6],
					status:       record[7],
					date:         record[8],
					iata:         record[9],
					coordinates:  record[10],
					remarks:      record[11],
				}

				// bypass records with "=" in field "change"
				if u.Change() == "=" {
					db.refs = append(db.refs, u)
					continue
				}
				// bypass some strange records
				if !u.IsCountry() && len(u.LocationCode()) == 0 {
					db.errors = append(db.errors, u)
					continue
				}
				// keep countries separate
				if u.IsCountry() {
					db.countries = append(db.countries, u)
					continue
				}
				// if duplicate record, copy existing record to "dups" and replace curent record in "locations"
				if existing, ok := db.pkey[u.Locode()]; ok == true {
					db.dups = append(db.dups, db.locations[existing])
					db.locations[existing] = u
					continue
				}
				// append new record
				db.locations = append(db.locations, u)
				// update primary key index
				db.pkey[u.Locode()] = len(db.locations) - 1
			}
		}
	}
}

func New() *DB {
	db := new(DB)
	db.pkey = make(map[string]int)
	db.load()
	return db
}

func (db *DB) CountTotal() int {
	return len(db.locations) + len(db.countries) + len(db.errors) + len(db.refs) + len(db.dups)
}

func (db *DB) CountLocations() int {
	return len(db.locations)
}

func (db *DB) CountCountries() int {
	return len(db.countries)
}

func (db *DB) CountErrors() int {
	return len(db.errors)
}

func (db *DB) CountRefs() int {
	return len(db.refs)
}

func (db *DB) CountDups() int {
	return len(db.dups)
}

func (db *DB) ReadLocation() (u unlocode, ok bool) {
	if db.locations_position < len(db.locations) {
		u = db.locations[db.locations_position]
		db.locations_position++
		return u, true
	}
	return u, false
}

func (db *DB) ReadCountry() (u unlocode, ok bool) {
	if db.countries_position < len(db.countries) {
		u = db.countries[db.countries_position]
		db.countries_position++
		return u, true
	}
	return u, false
}

func (u *unlocode) IsCountry() bool {
	if len(u.locationCode) == 0 && u.name[0:1] == "." && len(u.function) == 0 {
		return true
	}
	return false
}

func (u *unlocode) IsUnknown() bool {
	if len(u.function) > 0 && u.function[0:1] == "0" {
		return true
	}
	return false
}

func (u *unlocode) IsPort() bool {
	if len(u.function) > 0 && u.function[0:1] == "1" {
		return true
	}
	return false
}

func (u *unlocode) IsRail() bool {
	if len(u.function) > 0 && u.function[1:2] == "2" {
		return true
	}
	return false
}

func (u *unlocode) IsRoad() bool {
	if len(u.function) > 0 && u.function[2:3] == "3" {
		return true
	}
	return false
}

func (u *unlocode) IsAirport() bool {
	if len(u.function) > 0 && u.function[3:4] == "4" {
		return true
	}
	return false
}

func (u *unlocode) IsPostalOffice() bool {
	if len(u.function) > 0 && u.function[4:5] == "5" {
		return true
	}
	return false
}

func (u *unlocode) IsInland() bool {
	if len(u.function) > 0 && u.function[5:6] == "6" {
		return true
	}
	return false
}

func (u *unlocode) IsFixed() bool {
	if len(u.function) > 0 && u.function[6:7] == "7" {
		return true
	}
	return false
}

func (u *unlocode) IsBorder() bool {
	if len(u.function) > 0 && u.function[7:8] == "B" {
		return true
	}
	return false
}

func (u *unlocode) HasCoordinates() bool {
	if len(u.coordinates) > 0 {
		return true
	}
	return false
}

func (u *unlocode) Change() string {
	return u.change
}
func (u *unlocode) Locode() string {
	return u.countryCode + u.locationCode
}

func (u *unlocode) CountryCode() string {
	return u.countryCode
}

func (u *unlocode) LocationCode() string {
	return u.locationCode
}

func (u *unlocode) Name() string {
	if u.IsCountry() {
		return u.name[1:]
	} else {
		return u.name
	}
}

func (u *unlocode) NameSimple() string {
	return u.nameSimple
}

func (u *unlocode) Subdivision() string {
	return u.subdivision
}

func (u *unlocode) Function() string {
	return u.function
}

func (u *unlocode) Status() string {
	return u.status
}

func (u *unlocode) Date() string {
	return u.date
}

func (u *unlocode) Iata() string {
	return u.iata
}

func (u *unlocode) Coordinates() string {
	return u.coordinates
}

func (u *unlocode) Remarks() string {
	return u.remarks
}

func (u *unlocode) CoordinatesDD() (lat, lon float64) {

	c := u.Coordinates()
	lat = geotools.DMStoDD(c[0:2], c[2:4], "0", c[4:5])
	lon = geotools.DMStoDD(c[6:9], c[9:11], "0", c[11:12])
	return lat, lon
}
